# Notifications

The aim of this exercice is to build up a notification popin that will appear on the bottom right of the viewport.

Your code will expose an object in `window` providing one method with the following signature:

    Notifications.notify(msg[, time][, type]);

where

- msg: the message to be shown to the user (may be formatting HTML like bold, italics)
- time: amount of seconds to show the message (optional, defaults to 3 seconds)
- type: a value in \['info', 'warn', 'error'\] (optional, defaults to 'info')

Target browser: any modern browser and IE 8 and up. Visual degradation for is acceptable (IE8).

## Success criteria

The following results should be acheived.

### Look

A message is shown with the same look and feel than the screenshot using the defaults settings, as to be seen on this screenshot:

![Expected result](notifications.png "Expected result")


### Message types

The type of message is handled by setting the background color to signify the type (resp. white, yellow, red).

### Stackable

Message are stackable (most recent on top).

### Animation

The appearence and disappearence is animated (fading).

### Manual removal from stack

It is possible to remove a notification "manually"

    var loading = Notifications.notify(
        'Loading...', 
        1000  // set a very long time
        );
    
    // later
    loading.remove();    <-- this removes the "loading" notification.


